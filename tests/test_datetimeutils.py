#
# Tests for DateTimeUtils class
#
# Author: Carles Mateo
# Creation Date: 2014-01-02 10:00 GMT+1
# Last Update  : 2019-11-20 17:38 IST (Ireland)
#

import pytest
from lib.datetimeutils import DateTimeUtils


class TestDateTimeUtils(object):

    # Start Tests
    def test_get_datetime(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=False)

        assert s_datetime != ""
        assert len(s_datetime) == 19
        # @TODO: check more fields like dash separators, check that dates are in expected intervals

    def test_get_datetime_with_milliseconds(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=True)

        assert s_datetime != ""
        assert len(s_datetime) > 19
        assert s_datetime[19] == "."
        # @TODO: check more fields like dash separators, check that dates are in expected intervals

    def test_get_unix_epoch(self):
        o_datetime = DateTimeUtils()

        s_epoch = o_datetime.get_unix_epoch()
        assert s_epoch != ""
        assert len(s_epoch) >= 10
        assert int(s_epoch) > 1591210336
