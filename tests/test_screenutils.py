#
# Tests for ScreenUtils class
#
# Author: Carles Mateo
# Creation Date: 2014-01-01 10:11 GMT+1
# Last Update  : 2019-12-01 14:32 GMT+1
#

import pytest
from lib.screenutils import ScreenUtils


class TestScreenUtils(object):

    def test_p(self, capsys):
        s_text = "Text to print"

        o_screenutils = ScreenUtils()

        o_screenutils.p(s_text)

        o_captured = capsys.readouterr()
        assert o_captured.out == s_text + "\n"
        assert o_captured.err == ""

    def test_p_with_bw(self, capsys):
        s_text = "Text to print"

        o_screenutils = ScreenUtils(b_support_colors=False)

        o_screenutils.p(s_text)

        o_captured = capsys.readouterr()
        assert o_captured.out == s_text + "\n"
        assert o_captured.err == ""
